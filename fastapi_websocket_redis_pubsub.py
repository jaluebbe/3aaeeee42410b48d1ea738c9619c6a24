"""
Usage:

Make sure that redis is running on localhost (or adjust the url).
Install uvicorn

    pip install -u uvicorn

Install dependencies (redis needs to be larger/equal version 4.2.0rc1)

    pip install -u redis fastapi

Now just call

    python fastapi_websocket_redis_pubsub.py
   
Open two browser windows to the web interface  http://127.0.0.1:8000 or http://your_host_name_or_ip:8000

Enter some data in one window and it should appear in the other window.
   
"""

import asyncio

import logging


from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fastapi.websockets import WebSocket
from redis import asyncio as aioredis
import uvicorn

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


app = FastAPI()

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <body>
        <h1>WebSocket Chat</h1>
        <form action="" onsubmit="sendMessage(event)">
            <input type="text" id="messageText" autocomplete="off"/>
            <button>Send</button>
        </form>
        <ul id='messages'>
        </ul>
        <script>
            var ws = new WebSocket("ws://" + window.location.host + "/ws");
            ws.onmessage = function(event) {
                var messages = document.getElementById('messages')
                var message = document.createElement('li')
                var content = document.createTextNode(event.data)
                message.appendChild(content)
                messages.appendChild(message)
            };
            function sendMessage(event) {
                var input = document.getElementById("messageText")
                ws.send(input.value)
                input.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""


@app.get("/")
async def get():
    return HTMLResponse(html)


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    await redis_connector(websocket, source_channel="chat:c", target_channel="chat:c")


async def redis_connector(
    websocket: WebSocket,
    source_channel: str,
    target_channel: str,
    redis_uri: str = "redis://localhost:6379",
):
    async def consumer_handler(
        redis_connection: aioredis.client.Redis,
        websocket: WebSocket,
        target_channel: str,
    ):
        async for message in websocket.iter_text():
            await redis_connection.publish(target_channel, message)

    async def producer_handler(
        pubsub: aioredis.client.PubSub,
        websocket: WebSocket,
        source_channel: str,
    ):
        await pubsub.subscribe(source_channel)
        async for message in pubsub.listen():
            await websocket.send_text(message["data"])

    redis_connection = aioredis.from_url(redis_uri, decode_responses=True)
    pubsub = redis_connection.pubsub(ignore_subscribe_messages=True)
    consumer_task = asyncio.create_task(
        consumer_handler(redis_connection, websocket, target_channel)
    )
    producer_task = asyncio.create_task(
        producer_handler(pubsub, websocket, source_channel)
    )
    done, pending = await asyncio.wait(
        [consumer_task, producer_task], return_when=asyncio.FIRST_COMPLETED
    )
    logging.debug(f"Done task: {done}")
    for task in pending:
        logging.debug(f"Cancelling task: {task}")
        task.cancel()
    await redis_connection.close()
    
    
if __name__ == "__main__":
    # settting host to 0.0.0.0 allows access from other computers.
    # You may limit to access on the local machine by using 127.0.0.1 instead.
    uvicorn.run(app, host="0.0.0.0", port=8000)
